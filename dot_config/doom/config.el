;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;; Key bindings
(map! "C-#" #'comment-or-uncomment-region)
(map! "C-`" nil) ; unset toggle popup
(map! "C-x C-c" nil) ; don't quit with this chord.
;;(map! :leader
;;      (:when (modulep! :ui workspaces)
;;        (:prefix-map ("TAB" . "workspace")
;;         :desc "Create workspace"       "c"   #'+workspace/new
;;         :desc "Create named workspace" "C"   #'+workspace/new-named
;;         :desc "Next workspace"         "n"   #'+workspace/switch-right
;;         :desc "Previous workspace"     "p"   #'+workspace/switch-left
;;         "N" nil ;; this used to be new-named
;;         )))
(map! "C-S-i" #'+format/region-or-buffer)
(map! :leader
      (:prefix-map ("v" . "versioning")
       :desc "Magit switch branch"      "b"   #'magit-branch-checkout))
(map!
 "<f2>" #'+vterm/toggle
 :map vterm-mode-map
 "<f2>" #'+vterm/toggle)
(windmove-default-keybindings)
(map!
 "C-S-<left>" 'windmove-swap-states-left
 "C-S-<right>" 'windmove-swap-states-right
 "C-S-<up>" 'windmove-swap-states-up
 "C-S-<down>" 'windmove-swap-states-down)

;; Make whitespace visible
;; source: https://tech.tonyballantyne.com/2023/01/23/whitespace-mode/
(use-package! whitespace
  :config
  (setq
   whitespace-style '(face tabs tab-mark spaces space-mark trailing)
   whitespace-display-mappings '(
                                 (space-mark   ?\     [?·]     [?.])
                                 (space-mark   ?\xA0  [?¤]     [?_])
                                 ;; (newline-mark ?\n    [182 ?\n])
                                 (tab-mark     ?\t    [?» ?\t] [?\\ ?\t])))
  (global-whitespace-mode +1)
  (custom-set-faces
   `(whitespace-space ((t :foreground "lightgray")))))
;; No whitespace stuff in Magit.
;; Maybe make this more specific than magit-mode-hook
(add-hook 'magit-mode-hook (lambda () (setq-local whitespace-style nil)))

;; Show 80-character ruler
(add-hook 'prog-mode-hook #'display-fill-column-indicator-mode)

;; Show trailing spaces
(setq show-trailing-whitespace t)

;; Set face of active line number
(custom-set-faces!
  `(line-number-current-line :foreground "black" :weight
    semibold :inherit line-number))

;; Workspaces
(after! persp-mode
  (setq persp-emacsclient-init-frame-behaviour-override "main"))

;; Python LSP
(setq lsp-pylsp-plugins-flake8-enabled nil)
(setq lsp-pylsp-plugins-pylint-enabled t)
(setq lsp-pylsp-plugins-black-enabled t)
(setq lsp-pylsp-plugins-pydocstyle-enabled nil)

;; todotxt
(setq todotxt-file (file-truename "~/Nextcloud/todo-cie/todo.txt"))

;; vterm
; Re-enable when Xonsh works on Fedora...
;(setq vterm-shell "/usr/bin/xonsh")

;; org-roam
(make-directory "~/org-roam" t)
(setq org-roam-directory (file-truename "~/org-roam"))

;; org
(setq +org-capture-journal-file "journal.org")
(setq org-agenda-files
      '("~/Nextcloud/todo-cie" "~/Nextcloud/todo"))
(setq org-clocktable-defaults
      '(:scope agenda-with-archives
        :block today
        :maxlevel 3))
(setq org-clock-clocktable-default-properties
      '(
        :scope agenda-with-archives
        :block today)) ; TODO: somehow make this equal to '(format-time-string "%Y-%m-%d")
(setq org-log-done t)
(defun org-archive-done-tasks ()
  "Archive all tasks marked DONE in the file."
  (interactive)
  (require 'org-archive)
  (org-archive-all-done))
(map! :after org
      :map org-mode-map
      :localleader
      "X" #'org-archive-done-tasks)
;; clock
(setq org-clock-persist 'history)
(org-clock-persistence-insinuate)

;; spell
(after! ispell
  (ispell-set-spellchecker-params)
  (ispell-hunspell-add-multi-dic "eo,nl_BE,fr_BE,en_GB")
  (setq ispell-dictionary "eo,nl_BE,fr_BE,en_GB"))

;; project
(setq projectile-project-search-path '(("~/cie" . 3) ("~/Projets" . 3) ("~/Projektoj" . 3)))

;; xml
(setq nxml-slash-auto-complete-flag t)
;; when typing </, get </tag> instead of </tag>>.
;; (sp-local-pair 'nxml-mode "<" ">" :post-handlers '(("[d1]" "/")))

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
;; (setq user-full-name "John Doe"
;;       user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
(setq doom-font (font-spec :family "Fira Code" :size 13)
      doom-variable-pitch-font (font-spec :family "Noto Sans" :size 13)
      doom-big-font (font-spec :family "Fira Code" :size 20))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'leuven)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Nextcloud/org/")

;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
